---
layout: page
title: Métiers
---

Grâce à son expertise et celle de ses partenaires, Ultreia.io est, pour votre société, un interlocuteur capable
d'intervenir sur l'ensemble des problématiques de votre système d'information.

# Développement

Nous vous aidons à penser et à créer des applications ergonomiques adaptées à votre métier.
En vous équipant d'une application sur-mesure, vous vous offrez l'opportunité de vous libérer des tâches
improductives et fastidieuses afin de vous concentrer à nouveau sur ce qui fait votre cœur de métier et votre
valeur ajoutée. À vos côtés, dans la discussion et l'expérimentation, nous concevons brique par brique
l'application qui vous convient.

# Audit et conseil

Vos équipes sont confrontées à une technologie ou à une problématique spécifique ? Nos experts sont là pour vous
accompagner et tirer le meilleur de la situation.
Nos experts disposent d'un haut niveau d'expertise, reconnu par nos clients et par les communautés auxquelles
ils participent. Ils savent repérer les détails qui ont de l'importance et produire des rapports éclairés qui
  vous aident à prendre de la hauteur.

## Support et maintenance

Vous souhaitez pouvoir bénéficier d'experts pour accompagner ponctuellement vos développeurs. Nous fournissons
un support de niveau 3 autour d'un ensemble de logiciels et librairies libres afin de ne laisser aucune question
sans réponse. Vous avez fait développer une application, il faut désormais en assurer la maintenance et les
mises à jour de sécurité. Notre équipe de TMA intervient pour corriger dans les plus brefs délais les anomalies
rencontrées et maintenir, de manière pro-active, la sécurité de votre application (mises à jour de sécurité de
l'ensemble des composants de l'application).

## Les technologies que nous utilisons

Le cœur du métier d'Ultreia.io est le développement d'application avec le language de programmation Java. Java
est un language datant des années 90, il a su se renouveller durant toutes ces années en conservant une grande
stabilité. Ultreia.io a fait le choix depuis sa création de se spécialiser pour acquérir de fortes compétences
dans l'écosystème Java.

<div class="images">
    <a href="https://www.java.com" target="blank" title="Java"><img src="/img/techno/java.png"></a>
    <a href="http://hibernate.org" target="blank" title="Hibernate"><img src="/img/techno/hibernate.png"></a>
    <a href="https://spring.io" target="blank" title="Spring"><img src="/img/techno/spring.png"></a>
    <a href="https://www.apache.org" target="blank" title="Apache"><img src="/img/techno/apache.png"></a>
    <a href="https://maven.apache.org" target="blank" title="Maven"><img src="/img/techno/maven.png"></a>
</div>

Le futur des applications se passe de plus en plus sur internet. Il est dès à présent possible de réaliser des sites
web interractifs et ergonomiques. Ultreia.io maîtrise ces technologies qui sont en constante évolutions.

<div class="images">
    <a href="https://www.javascript.com" target="blank" title="Javascript"><img src="/img/techno/javascript.png"></a>
    <a href="https://nodejs.org" target="blank" title="NodeJs"><img src="/img/techno/node.png"></a>
    <a href="https://angular.io" target="blank" title="Angular"><img src="/img/techno/angular.png"></a>
    <a href="https://reactjs.org" target="blank" title="ReactJs"><img src="/img/techno/react.png"></a>
    <a href="https://webpack.js.org" target="blank" title="Webpack"><img src="/img/techno/webpack.png"></a>
</div>

Nous prenons appui sur des outils et des plateformes éprouvés afin de faciliter l'exploitation sur
l'infrastructure de nos clients ou celle de nos partenaires spécialistes.

<div class="images">
    <a href="https://www.docker.com" target="blank" title="Docker"><img src="/img/techno/docker.png"></a>
    <a href="https://www.postgresql.org" target="blank" title="Postgresql"><img src="/img/techno/postgresql.png"></a>
    <a href="https://tomcat.apache.org" target="blank" title="Tomcat"><img src="/img/techno/tomcat.png"></a>
    <a href="https://git-scm.com" target="blank" title="Git"><img src="/img/techno/git.png"></a>
    <a href="https://gitlab.com" target="blank" title="GitLab"><img src="/img/techno/gitlab.png"></a>
</div>

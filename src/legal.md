---
layout: page
title: Mentions légales
---

# Généralités

<div class="columns-2">
    <div class="col" style="flex-grow: 1; display: flex; flex-flow: column wrap;align-items: stretch">
        <div style="flex-grow: 1">
            <h3>Propriété et responsabilité éditoriale</h3>
            <p>Le présent site est la propriété de la société Ultreia.io</p>
            <p><strong>Ultreia.io<br>18C Rue bonne garde<br>44230 Saint-Sébastien-sur-Loire</strong></p>
        </div>
        <div>
            <h3>Responsable technique</h3>
            <p>Le présent site est maintenu par la société Ultreia.io.</p>
        </div>
    </div>
    <div class="col">
        <h3>Hébergement du site</h3>
        <p>L'hébergement du présent site est assuré par 1and1.</p>
        <p><strong>1and1<br>7, place de la Gare
            <br>
            BP 70109
            <br>
            57201 Sarreguemines Cedex <br>France<br>Site web&nbsp;:
            <a href="https://www.1and1.fr">www.1and1.fr</a></strong></p>
        <h3>Conception et réalisation</h3>
        <p>Le design du présent site a été conçu par la société Ultreia.io</p></div>
</div>

# Données personnelles

## Données nominatives

Conformément à l’article 34 de la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux
fichiers et aux libertés, l’utilisateur dispose d’un droit d’accès, de modification, de
rectification et de suppression des données qui le concernent. Pour l’exercer, l’utilisateur
peut nous contacter.

## Cookies

L’utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer
automatiquement sur son logiciel de navigation. Un cookie est un élément qui ne permet pas
d’identifier l’utilisateur mais sert à enregistrer des informations relatives à la navigation de
celui-ci sur le site Internet (analyser le trafic du site par exemple). L’utilisateur pourra
désactiver ce cookie par l’intermédiaire des paramètres figurant au sein de son logiciel de
navigation.

## Liens hypertextes

Le présent site peut contenir des liens hypertextes vers des sites tiers, partenaires ou non. Ces
sites ne sont pas sous le contrôle de la société Ultreia.io qui décline toute responsabilité
quant au contenu de ces sites, des mises-à-jour qui pourraient leur être apportées ou de toute
communication à votre encontre émise par lesdits sites. L’insertion de tels liens dans le site
n’implique en aucun cas quelque approbation de leur contenu par la société Ultreia.io.

# Propriété intellectuelle

Le site Internet, sa structure générale, ainsi que les textes, images animées ou non,
savoir-faire, dessins, graphismes (…) et tout autre élément composant le site, sont la propriété
de la société Ultreia.io et de leurs auteurs respectifs. Toute représentation totale ou
partielle de ce site par quelque procédé que ce soit, sans l’autorisation expresse de
l’exploitant du site Internet est interdite et constituerait une contrefaçon sanctionnée par les
articles L 335-2 et suivants du Code de la propriété intellectuelle. Les marques de l’exploitant
du site Internet et de ses partenaires, ainsi que les logos figurant sur le site sont des
marques (semi-figuratives ou non) déposées. Toute reproduction totale ou partielle de ces
marques ou de ces logos, effectuée à partir des éléments du site sans l’autorisation expresse de
l’exploitant du site Internet ou de son ayant-droit est donc prohibée, au sens de l’article
L713-2 du Code de la Propriété Intellectuelle.

Nous vous remercions de nous faire part d’éventuelles omissions, erreurs, corrections, en nous
contactant.

# Crédits photos

Gérard Chemit.

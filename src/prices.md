---
layout: page
title: Tarifs
---

Par soucis de transparence, Ultreia.io publie ses tarifs.

Un remise commerciale de 10% est accordée à tout développement placé sous licence libre et lorsque nous
sommes associés au copyright.

Tous nos tarifs sont hors taxe et journalier sauf mention contraire.
        
# Contrat de développement
Ultreia.io a fait le choix d'adopter un tarif unique pour toute prestation de 
développement de 600€.

# Contrat d'audit, de conseil ou d'ingénerie
Les prestations d'audit, de conseil ou d'ingénerie sont au tarif de 800€.
# Support (TMA)

Les prestations de support sont facturées à l'heure consommée sur une base de 100€.
